package org.elegant.aash.comparator;

import static org.junit.Assert.assertThat;

import java.io.IOException;

import org.elegant.aash.comparator.model.Model;
import org.elegant.aash.comparator.tools.CompareTools;
import org.hamcrest.CoreMatchers;
import org.junit.Test;

public class InclusionCompareEngineTest {

	@Test
	public void testSimpleComparison() throws IOException {
		Model m1 = new Model("titi");
		m1.setDerived(new Model("titi"));
		Model m2 = CompareTools.deepCopy(m1);
		CompareEngine<Model> compEngine = CompareEngineBuilder.forType(Model.class)
				.includeProperty("derived.name")
				.includeProperty("children[0].name")
				.build();
		
		assertThat(compEngine.inspectChanges(m1, m2), CoreMatchers.equalTo(false));
		m1.setName("toto");
		// Root level name is not inspected, it will not trigger changes
		assertThat(compEngine.inspectChanges(m1, m2), CoreMatchers.equalTo(false));
		m1.getDerived().setName("tutu");
		assertThat(compEngine.inspectChanges(m1, m2), CoreMatchers.equalTo(true));
	}

	@Test
	public void testPropertyPrefixedComparison() throws IOException {
		Model m1 = new Model(null);
		Model m2 = new Model("titi");
		CompareEngine<Model> compEngine = CompareEngineBuilder.forType(Model.class)
				.includeProperty("<name")
				.build();
		assertThat(compEngine.inspectChanges(m1, m2), CoreMatchers.equalTo(false));
		m1.setName("toto");
		assertThat(compEngine.inspectChanges(m1, m2), CoreMatchers.equalTo(true));
		compEngine = CompareEngineBuilder.forType(Model.class)
				.includeProperty(">name")
				.build();
		m2.setName(null);
		assertThat(compEngine.inspectChanges(m1, m2), CoreMatchers.equalTo(false));
		m2.setName("titi");
		assertThat(compEngine.inspectChanges(m1, m2), CoreMatchers.equalTo(true));
	}
}
