package org.elegant.aash.comparator;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.elegant.aash.comparator.model.Model;
import org.elegant.aash.comparator.tools.CompareTools;
import org.junit.Test;

import com.google.common.base.Stopwatch;
import com.google.common.collect.Lists;

public class InclusionCompareEnginePerfTest {

	private int MAX_LOOP = 10000;

	@Test
	public void perfCompareWithSameEngine() throws IOException {
		List<Boolean> models = Lists.newArrayList();
		Model m1 = new Model("titi");
		m1.setDerived(new Model("titi"));
		Model m2 = CompareTools.deepCopy(m1);
		CompareEngine<Model> compEngine = CompareEngineBuilder.forType(Model.class).includeProperty("derived.name").includeProperty("children[0].name")
				.build();
		long moyParsing = 0;
		Stopwatch sw = new Stopwatch();
		for (int i = 0; i < MAX_LOOP; i++) {
			if (i % 2 == 0) {
				m2.getDerived().setName("toto");
			} else {
				m2.getDerived().setName("titi");
			}
			sw.reset().start();
			models.add(compEngine.inspectChanges(m1, m2));
			if (i != 0) {
				moyParsing += sw.elapsedTime(TimeUnit.MICROSECONDS);
			}
//			System.out.print("Inspect Changes [" + i + "] : " + sw.elapsedTime(TimeUnit.MICROSECONDS) + " mus\n");
		}
		moyParsing /= (MAX_LOOP - 1);
		System.out.println("\n\n Moy Inspect Changes " + moyParsing + " mus ");
//		System.out.println("\n\n" + models.size());
	}
}
