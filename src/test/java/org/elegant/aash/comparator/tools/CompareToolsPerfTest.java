package org.elegant.aash.comparator.tools;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.elegant.aash.comparator.model.Model;
import org.junit.Test;

import com.google.common.base.Stopwatch;
import com.google.common.collect.Lists;

public class CompareToolsPerfTest {

	private int MAX_LOOP = 10000;

	@Test
	public void perfDeepCopy() throws IOException {
		List<Model> models = Lists.newArrayList();
		Model m1 = new Model("titi");
		m1.setDerived(new Model("titi"));
		long moyParsing = 0;
		Stopwatch sw = new Stopwatch();
		for (int i = 0; i < MAX_LOOP; i++) {
			sw.reset().start();
			models.add(CompareTools.deepCopy(m1));
			if (i != 0) {
				moyParsing += sw.elapsedTime(TimeUnit.MICROSECONDS);
			}
//			System.out.print("DeepCopy [" + i + "] : " + sw.elapsedTime(TimeUnit.MICROSECONDS) + " mus\n");
		}
		moyParsing /= (MAX_LOOP - 1);
		System.out.println("\n\n Moy DeepCopy " + moyParsing + " mus ");
//		System.out.println("\n\n" + models.size());
	}
}
