package org.elegant.aash.comparator.parsing;

import static org.junit.Assert.assertThat;

import java.io.IOException;

import org.elegant.aash.comparator.model.Model;
import org.elegant.aash.comparator.parsing.expr.DottedExpression;
import org.elegant.aash.comparator.parsing.visitor.PropertyExpressionVisitor;
import org.elegant.aash.comparator.tools.CompareTools;
import org.hamcrest.CoreMatchers;
import org.junit.Test;

import com.google.common.collect.Lists;

public class CompareExpressionParserTest {

	@Test
	public void testVisitSimpleDottedExpression() throws IOException {
		String sText = "bytes";
		DottedExpression expr = ExpressionParser.parse(sText);
		PropertyExpressionVisitor visitor = PropertyExpressionVisitor.of("titi", "toto");
		expr.accept(visitor);
		assertThat(visitor.getGraph().isChanged(), CoreMatchers.equalTo(true));
		visitor = PropertyExpressionVisitor.of("titi", "titi");
		expr.accept(visitor);
		assertThat(visitor.getGraph().isChanged(), CoreMatchers.equalTo(false));
	}

	@Test
	public void testVisitMultipleDottedExpression() throws IOException {
		String sText = "derived.name";
		DottedExpression expr = ExpressionParser.parse(sText);
		Model m1 = new Model("titi");
		m1.setDerived(new Model("titi"));
		Model m2 = new Model("titi");
		m2.setDerived(new Model("titi"));
		PropertyExpressionVisitor visitor = PropertyExpressionVisitor.of(m1, m2);
		expr.accept(visitor);
		assertThat(visitor.getGraph().isChanged(), CoreMatchers.equalTo(false));
		m2.setDerived(new Model("toto"));
		visitor = PropertyExpressionVisitor.of(m1, m2);
		expr.accept(visitor);
		assertThat(visitor.getGraph().isChanged(), CoreMatchers.equalTo(true));
	}

	@Test
	public void testVisitArrayIntegerExpression() throws IOException {
		String sText = "children[0].name";
		DottedExpression expr = ExpressionParser.parse(sText);
		Model m1 = new Model("titi");
		Model m2 = new Model("titi");
		PropertyExpressionVisitor visitor = PropertyExpressionVisitor.of(m1, m2);
		expr.accept(visitor);
		assertThat(visitor.getGraph().isChanged(), CoreMatchers.equalTo(false));
		m1.setChildren(Lists.newArrayList(new Model("tutu")));
		visitor = PropertyExpressionVisitor.of(m1, m2);
		expr.accept(visitor);
		assertThat(visitor.getGraph().isChanged(), CoreMatchers.equalTo(true));
	}

	@Test
	public void testVisitArrayStarExpression() throws IOException {
		String sText = "children[*].name";
		DottedExpression expr = ExpressionParser.parse(sText);
		Model m1 = new Model("titi");
		m1.setChildren(Lists.newArrayList(new Model("tutu")));
		Model m2 = CompareTools.deepCopy(m1);
		PropertyExpressionVisitor visitor = PropertyExpressionVisitor.of(m1, m2);
		expr.accept(visitor);
		assertThat(visitor.getGraph().isChanged(), CoreMatchers.equalTo(false));
		m2.getChildren().get(0).setName("toto");
		visitor = PropertyExpressionVisitor.of(m1, m2);
		expr.accept(visitor);
		assertThat(visitor.getGraph().isChanged(), CoreMatchers.equalTo(true));
		m2.getChildren().get(0).setName("tutu");
		visitor = PropertyExpressionVisitor.of(m1, m2);
		expr.accept(visitor);
		assertThat(visitor.getGraph().isChanged(), CoreMatchers.equalTo(false));
		m2.getChildren().add(new Model("toto"));
		visitor = PropertyExpressionVisitor.of(m1, m2);
		expr.accept(visitor);
		assertThat(visitor.getGraph().isChanged(), CoreMatchers.equalTo(true));
	}
}
