package org.elegant.aash.comparator.parsing;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.elegant.aash.comparator.parsing.expr.DottedExpression;
import org.junit.Test;

import com.google.common.base.Stopwatch;
import com.google.common.collect.Lists;

public class CompareExpressionParserPerfTest {

	private int MAX_LOOP = 10000;

	@Test
	public void perfParseExpression() throws IOException {
		List<String> exprTs = Lists.newArrayList();
		long moyParsing = 0;
		long moyToString = 0;
		String sText = "userTruc.maTable[variable.attribute == false | (toto > 18 & userTruc[*].family in [\"toto\", \"titi\"])].tableau[length(texte)].name";
		Stopwatch sw = new Stopwatch();
		for (int i = 0; i < MAX_LOOP; i++) {
			sw.reset().start();
			DottedExpression expr = ExpressionParser.parse(sText);
			if (i != 0) {
				moyParsing += sw.elapsedTime(TimeUnit.MICROSECONDS);
			}
//			System.out.print("Parsing [" + i + "] : " + sw.elapsedTime(TimeUnit.MICROSECONDS) + " mus");
			sw.reset().start();
			exprTs.add(expr.toString());
			if (i != 0) {
				moyToString += sw.elapsedTime(TimeUnit.MICROSECONDS);
			}
//			System.out.println(" / toString : " + sw.elapsedTime(TimeUnit.MICROSECONDS) + " ms ");
		}
		moyParsing /= (MAX_LOOP - 1);
		moyToString /= (MAX_LOOP - 1);
		System.out.println("\n\n Moy Parsing " + moyParsing + " mus / Moy toString " + moyToString + " mus");
//		System.out.println("\n\n" + exprTs.size());
	}
}
