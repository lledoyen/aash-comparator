package org.elegant.aash.comparator.model;

import java.io.Serializable;
import java.util.List;

public class Model implements Serializable {

	private static final long serialVersionUID = 1L;

	private String name;
	private Model derived;
	private List<Model> children;
	private Model[] derivedArray;

	public Model(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Model getDerived() {
		return derived;
	}

	public void setDerived(Model derived) {
		this.derived = derived;
	}

	public List<Model> getChildren() {
		return children;
	}

	public void setChildren(List<Model> children) {
		this.children = children;
	}

	public Model[] getDerivedArray() {
		return derivedArray;
	}

	public void setDerivedArray(Model[] derivedArray) {
		this.derivedArray = derivedArray;
	}
}
