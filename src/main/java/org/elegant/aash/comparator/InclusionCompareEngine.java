package org.elegant.aash.comparator;

import java.util.List;

public class InclusionCompareEngine<T> implements CompareEngine<T> {

	private Class<T> clazz;
	private List<PropertyDescriptor<T>> propertyDescriptors;

	InclusionCompareEngine(Class<T> clazz, List<PropertyDescriptor<T>> propertyDescriptors) {
		this.clazz = clazz;
		this.propertyDescriptors = propertyDescriptors;
	}

	public boolean inspectChanges(T oldBean, T newBean) {
		boolean result = false;
		for (PropertyDescriptor<T> propertyDescriptor : propertyDescriptors) {
			result |= propertyDescriptor.inspectChanges(oldBean, newBean);
		}
		return result;
	}

	public Class<T> getClazz() {
		return clazz;
	}
}
