package org.elegant.aash.comparator;

public interface CompareEngine<T> {

	boolean inspectChanges(T oldBean, T newBean);
}
