package org.elegant.aash.comparator.tools;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class Objects {

	public static <T> boolean equals(T a, T b) {
		if (a instanceof boolean[]) {
			return Arrays.equals((boolean[]) a, (boolean[]) b);
		} else if (a instanceof byte[]) {
			return Arrays.equals((byte[]) a, (byte[]) b);
		} else if (a instanceof char[]) {
			return Arrays.equals((char[]) a, (char[]) b);
		} else if (a instanceof double[]) {
			return Arrays.equals((double[]) a, (double[]) b);
		} else if (a instanceof float[]) {
			return Arrays.equals((float[]) a, (float[]) b);
		} else if (a instanceof int[]) {
			return Arrays.equals((int[]) a, (int[]) b);
		} else if (a instanceof long[]) {
			return Arrays.equals((long[]) a, (long[]) b);
		} else if (a instanceof short[]) {
			return Arrays.equals((short[]) a, (short[]) b);
		} else if (a instanceof Object[]) {
			return Arrays.deepEquals((Object[]) a, (Object[]) b);
		}
		return a == b || (a != null && a.equals(b));
	}

	public static int safeMaxSize(Object oldObject, Object newObject) {
		return Math.max(safeSize(oldObject), safeSize(newObject));
	}

	public static int safeSize(Object o) {
		// Optimization for Collection which have a size method
		if (o instanceof Collection) {
			return ((Collection<?>) o).size();
		} else if (o instanceof Iterable) {
			List<?> intermediary = Lists.newArrayList((Iterable<?>) o);
			return intermediary.size();
		} else if (o instanceof Object[]) {
			return ((Object[]) o).length;
		} else if (o instanceof byte[]) {
			return ((byte[]) o).length;
		} else if (o instanceof short[]) {
			return ((short[]) o).length;
		} else if (o instanceof int[]) {
			return ((int[]) o).length;
		} else if (o instanceof long[]) {
			return ((long[]) o).length;
		} else if (o instanceof float[]) {
			return ((float[]) o).length;
		} else if (o instanceof double[]) {
			return ((double[]) o).length;
		} else if (o instanceof char[]) {
			return ((char[]) o).length;
		} else if (o instanceof boolean[]) {
			return ((boolean[]) o).length;
		} else {
			throw new IllegalStateException(
					"Not an available Array or Iterable");
		}
	}

	public static Object safeGet(Object newObject, int index) {
		// Optimization for List which have a get method
		if (newObject instanceof List && ((List<?>) newObject).size() > index) {
			return ((List<?>) newObject).get(index);
		} else if (newObject instanceof Iterable) {
			List<?> intermediary = Lists.newArrayList((Iterable<?>) newObject);
			if (intermediary.size() > index) {
				return intermediary.get(index);
			} else {
				return null;
			}
		} else if (newObject instanceof Object[]
				&& ((Object[]) newObject).length > index) {
			return ((Object[]) newObject)[index];
		} else if (newObject instanceof byte[]
				&& ((byte[]) newObject).length > index) {
			return ((byte[]) newObject)[index];
		} else if (newObject instanceof short[]
				&& ((short[]) newObject).length > index) {
			return ((short[]) newObject)[index];
		} else if (newObject instanceof int[]
				&& ((int[]) newObject).length > index) {
			return ((int[]) newObject)[index];
		} else if (newObject instanceof long[]
				&& ((long[]) newObject).length > index) {
			return ((long[]) newObject)[index];
		} else if (newObject instanceof float[]
				&& ((float[]) newObject).length > index) {
			return ((float[]) newObject)[index];
		} else if (newObject instanceof double[]
				&& ((double[]) newObject).length > index) {
			return ((double[]) newObject)[index];
		} else if (newObject instanceof char[]
				&& ((char[]) newObject).length > index) {
			return ((char[]) newObject)[index];
		} else if (newObject instanceof boolean[]
				&& ((boolean[]) newObject).length > index) {
			return ((boolean[]) newObject)[index];
		} else {
			return null;
		}
	}
}
