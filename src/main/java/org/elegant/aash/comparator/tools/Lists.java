package org.elegant.aash.comparator.tools;

import java.util.ArrayList;
import java.util.List;

public class Lists {

	public static <T> List<T> newArrayList(T... objs) {
		List<T> result = new ArrayList<T>();
		for(T obj : objs) {
			result.add(obj);
		}
		return result;
	}

	public static <T> List<T> newArrayList(Iterable<T> it) {
		List<T> result = new ArrayList<T>();
		for(T obj : it) {
			result.add(obj);
		}
		return result;
	}
}
