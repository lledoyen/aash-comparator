package org.elegant.aash.comparator.tools;

public class Booleans {

	public static boolean and(boolean bValue, boolean... bValues) {
		boolean result = bValue;
		for(boolean b : bValues) {
			result &= b;
		}
		return result;
	}
}
