package org.elegant.aash.comparator;

import java.io.IOException;

import org.elegant.aash.comparator.parsing.ExpressionParser;
import org.elegant.aash.comparator.parsing.expr.DottedExpression;
import org.elegant.aash.comparator.parsing.visitor.PropertyExpressionVisitor;

public class PropertyDescriptor<T> {

	private String description;
	private DottedExpression expr;
	private boolean forgetOldNull;
	private boolean forgetNewNull;

	PropertyDescriptor(String _description) throws IOException {
		if(_description.startsWith("<")) {
			forgetOldNull = true;
			this.description = _description.substring(1);
		} else if(_description.startsWith(">")) {
			forgetNewNull = true;
			this.description = _description.substring(1);
		} else {
			this.description = _description;
		}
		this.expr = ExpressionParser.parse(this.description);
	}

	public String getDescription() {
		return description;
	}

	boolean inspectChanges(T oldBean, T newBean) {
		PropertyExpressionVisitor visitor = PropertyExpressionVisitor.of(oldBean, newBean);
		expr.accept(visitor);
		return visitor.getGraph().isChanged(forgetOldNull, forgetNewNull);
	}

	public boolean isForgetOldNull() {
		return forgetOldNull;
	}

	public boolean isForgetNewNull() {
		return forgetNewNull;
	}
}
