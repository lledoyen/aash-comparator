package org.elegant.aash.comparator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CompareEngineBuilder<T> {

	private Class<T> clazz;

	private List<PropertyDescriptor<T>> includePropertyDescriptors = new ArrayList<PropertyDescriptor<T>>();
	private List<PropertyDescriptor<T>> excludePropertyDescriptors = new ArrayList<PropertyDescriptor<T>>();

	private CompareEngineBuilder(Class<T> clazz) {
		this.clazz = clazz;
	}

	public static <T> CompareEngineBuilder<T> forType(Class<T> clazz) {
		return new CompareEngineBuilder<T>(clazz);
	}

	public CompareEngineBuilder<T> includeProperty(String property) {
		try {
			includePropertyDescriptors.add(new PropertyDescriptor<T>(property));
		} catch (IOException e) {
			throw new IllegalArgumentException("Invalid property : ", e);
		}
		return this;
	}

	public CompareEngineBuilder<T> excludeProperty(String property) {
		try {
			excludePropertyDescriptors.add(new PropertyDescriptor<T>(property));
		} catch (IOException e) {
			throw new IllegalArgumentException("Invalid property : ", e);
		}
		return this;
	}

	public CompareEngine<T> build() {
		if(includePropertyDescriptors.size() > 0) {
			return new InclusionCompareEngine<T>(clazz, includePropertyDescriptors);
		} else {
			return new ExclusionCompareEngine<T>(clazz, excludePropertyDescriptors);
		}
	}
}
