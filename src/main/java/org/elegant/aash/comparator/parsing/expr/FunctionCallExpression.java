package org.elegant.aash.comparator.parsing.expr;

import java.util.ArrayList;
import java.util.List;

import org.elegant.aash.comparator.parsing.visitor.ExpressionVisitor;

public class FunctionCallExpression extends Expression {
	private String _sFunctionName;
	private List<Expression> _parameters = new ArrayList<Expression>();

	public FunctionCallExpression(String sFunctionName) {
		_sFunctionName = sFunctionName;
	}

	public void addParameter(Expression expr) {
		_parameters.add(expr);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(_sFunctionName);
		sb.append('(');
		boolean bFirst = true;
		for (Expression item : _parameters) {
			if (bFirst) {
				bFirst = false;
			} else {
				sb.append(", ");
			}
			sb.append(item.toString());
		}
		sb.append(')');
		return sb.toString();
	}

	@Override
	public void accept(ExpressionVisitor visitor) {
		visitor.visit(this);
	}

}
