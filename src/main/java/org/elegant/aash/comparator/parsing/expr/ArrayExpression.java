package org.elegant.aash.comparator.parsing.expr;

import java.util.ArrayList;
import java.util.List;

import org.elegant.aash.comparator.parsing.visitor.ExpressionVisitor;

public class ArrayExpression extends Expression {

	private List<Expression> _items = new ArrayList<Expression>();

	public ArrayExpression() {
	}

	public void addItem(Expression expr) {
		_items.add(expr);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append('[');
		boolean bFirst = true;
		for (Expression item : _items) {
			if (bFirst) {
				bFirst = false;
			} else {
				sb.append(", ");
			}
			sb.append(item.toString());
		}
		sb.append(']');
		return sb.toString();
	}

	@Override
	public void accept(ExpressionVisitor visitor) {
		visitor.visit(this);
	}
}
