package org.elegant.aash.comparator.parsing.visitor;

import org.elegant.aash.comparator.parsing.expr.ArithmeticExpression;
import org.elegant.aash.comparator.parsing.expr.ArrayExpression;
import org.elegant.aash.comparator.parsing.expr.BooleanExpression;
import org.elegant.aash.comparator.parsing.expr.FunctionCallExpression;
import org.elegant.aash.comparator.parsing.expr.NotExpression;
import org.elegant.aash.comparator.parsing.expr.literal.BooleanLiteral;
import org.elegant.aash.comparator.parsing.expr.literal.DoubleLiteral;
import org.elegant.aash.comparator.parsing.expr.literal.StarLiteral;
import org.elegant.aash.comparator.parsing.expr.literal.StringLiteral;

public interface ExpressionVisitor extends DottedExpressionVisitor {

	void visit(ArrayExpression compareArrayExpression);

	void visit(FunctionCallExpression compareFunctionCallExpression);

	void visit(NotExpression compareNotExpression);

	void visit(BooleanLiteral compareBooleanLiteral);

	void visit(DoubleLiteral compareDoubleLiteral);

	void visit(StarLiteral compareStarLiteral);

	void visit(StringLiteral compareStringLiteral);

	void visit(BooleanExpression compareBooleanExpression);

	void visit(ArithmeticExpression compareArithmeticExpression);
}
