package org.elegant.aash.comparator.parsing.visitor.model;

import java.util.List;

import org.elegant.aash.comparator.tools.Objects;

public class PropertyElementValue {

	// Initial values
	private Object _oldInitialValue;
	private Object _newInitialValue;
	/** Class of these initial values except when they are both null. */
	private Class<?> _cClazz;

	// targeted values description
	private String _sVarName;
	private String _sCondition;
	
	// targeted values element
	private List<PropertyElementValue> _nextElements;

	public PropertyElementValue(Object oldBean, Object newBean) {
		this._oldInitialValue = oldBean;
		this._newInitialValue = newBean;
	}

	public List<PropertyElementValue> getNext() {
		return _nextElements;
	}

	public void setNext(List<PropertyElementValue> nextElement) {
		_nextElements = nextElement;
	}

	public String getVarName() {
		return _sVarName;
	}

	public void setVarName(String sVarName) {
		_sVarName = sVarName;
	}

	public String getCondition() {
		return _sCondition;
	}

	public void setCondition(String sCondition) {
		_sCondition = sCondition;
	}

	public Object getOldInitialValue() {
		return _oldInitialValue;
	}

	public void setOldInitialValue(Object oldValue) {
		_oldInitialValue = oldValue;
	}

	public Object getNewInitialValue() {
		return _newInitialValue;
	}

	public void setNewInitialValue(Object _newValue) {
		this._newInitialValue = _newValue;
	}

	public void setClazz(Class<?> clazz) {
		_cClazz = clazz;
	}

	public Class<?> getClazz() {
		return _cClazz;
	}

	public boolean isChanged() {
		return isChanged(false, false);
	}
	public boolean isChanged(boolean forgetOldNull, boolean forgetNewNull) {
		boolean result = false;
		if(_nextElements != null) {
			result = false;
			for(PropertyElementValue next : _nextElements) {
				result |= next.isChanged(forgetOldNull, forgetNewNull);
			}
		} else {
			if(forgetOldNull && _oldInitialValue == null) {
				result = false;
			} else if(forgetNewNull && _newInitialValue == null) {
				result = false;
			} else {
				result = !Objects.equals(_oldInitialValue, _newInitialValue);
			}
		}
		return result;
	}
}
