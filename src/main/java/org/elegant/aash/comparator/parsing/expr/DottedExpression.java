package org.elegant.aash.comparator.parsing.expr;

import org.elegant.aash.comparator.parsing.visitor.DottedExpressionVisitor;
import org.elegant.aash.comparator.parsing.visitor.ExpressionVisitor;

public class DottedExpression extends Expression {
	private String _sVarName;
	private Expression _indexExpr;
	private DottedExpression _nextExpr;

	public DottedExpression(String sVarName) {
		_sVarName = sVarName;
	}

	public String getVariableName() {
		return _sVarName;
	}

	public Expression getIndex() {
		return _indexExpr;
	}

	public void setIndex(Expression indexExpr) {
		_indexExpr = indexExpr;
	}

	public DottedExpression getNext() {
		return _nextExpr;
	}

	public void setNext(DottedExpression nextExpr) {
		_nextExpr = nextExpr;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(_sVarName);
		if (_indexExpr != null) {
			sb.append('[');
			sb.append(_indexExpr.toString());
			sb.append(']');
		}
		if (_nextExpr != null) {
			sb.append('.');
			sb.append(_nextExpr.toString());
		}
		return sb.toString();
	}

	@Override
	public void accept(ExpressionVisitor visitor) {
		visitor.visit(this);
	}

	public void accept(DottedExpressionVisitor visitor) {
		visitor.visit(this);
	}
}
