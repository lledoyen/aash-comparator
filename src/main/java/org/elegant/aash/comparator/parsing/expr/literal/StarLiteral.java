package org.elegant.aash.comparator.parsing.expr.literal;

import org.elegant.aash.comparator.parsing.expr.Expression;
import org.elegant.aash.comparator.parsing.visitor.ExpressionVisitor;

public class StarLiteral extends Expression {
	public StarLiteral() {
	}

	@Override
	public String toString() {
		return "*";
	}

	@Override
	public void accept(ExpressionVisitor visitor) {
		visitor.visit(this);
	}

}
