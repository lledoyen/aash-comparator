package org.elegant.aash.comparator.parsing.expr;

import org.elegant.aash.comparator.parsing.visitor.ExpressionVisitor;

public class ArithmeticExpression extends BinaryExpression {
	public ArithmeticExpression(Expression left, char cOp, Expression right) {
		super(left, cOp, right);
	}

	@Override
	public void accept(ExpressionVisitor visitor) {
		visitor.visit(this);
	}
}
