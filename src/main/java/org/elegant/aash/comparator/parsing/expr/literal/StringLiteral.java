package org.elegant.aash.comparator.parsing.expr.literal;

import org.elegant.aash.comparator.parsing.expr.Expression;
import org.elegant.aash.comparator.parsing.visitor.ExpressionVisitor;

public class StringLiteral extends Expression {
	private String _sText;

	public StringLiteral(String sText) {
		_sText = sText;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append('"');
		for (char c : _sText.toCharArray()) {
			if (c == '\\') {
				sb.append("\\\\");
			} else if (c == '\r') {
				sb.append("\\r");
			} else if (c == '\n') {
				sb.append("\\n");
			} else if (c == '\t') {
				sb.append("\\t");
			} else {
				sb.append(c);
			}
		}
		sb.append('"');
		return sb.toString();
	}

	@Override
	public void accept(ExpressionVisitor visitor) {
		visitor.visit(this);
	}
}
