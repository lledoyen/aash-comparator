package org.elegant.aash.comparator.parsing.expr;

import org.elegant.aash.comparator.parsing.visitor.ExpressionVisitor;

public class NotExpression extends Expression {
	private Expression _expr;

	public NotExpression(Expression expr) {
		_expr = expr;
	}

	@Override
	public String toString() {
		return "!" + _expr;
	}

	@Override
	public void accept(ExpressionVisitor visitor) {
		visitor.visit(this);
	}
}
