package org.elegant.aash.comparator.parsing.expr.literal;

import org.elegant.aash.comparator.parsing.expr.Expression;
import org.elegant.aash.comparator.parsing.visitor.ExpressionVisitor;

public class DoubleLiteral extends Expression {
	private double _dValue;

	public DoubleLiteral(double dValue) {
		_dValue = dValue;
	}

	public double getValue() {
		return _dValue;
	}

	@Override
	public String toString() {
		return String.valueOf(_dValue);
	}

	@Override
	public void accept(ExpressionVisitor visitor) {
		visitor.visit(this);
	}

}
