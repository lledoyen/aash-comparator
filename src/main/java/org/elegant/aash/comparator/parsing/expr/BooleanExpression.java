package org.elegant.aash.comparator.parsing.expr;

import org.elegant.aash.comparator.parsing.visitor.ExpressionVisitor;

public class BooleanExpression extends BinaryExpression {
	public BooleanExpression(Expression left, char cOp, Expression right) {
		super(left, cOp, right);
	}

	@Override
	public void accept(ExpressionVisitor visitor) {
		visitor.visit(this);
	}
}
