package org.elegant.aash.comparator.parsing.expr.literal;

import org.elegant.aash.comparator.parsing.expr.Expression;
import org.elegant.aash.comparator.parsing.visitor.ExpressionVisitor;

public class BooleanLiteral extends Expression {
	private boolean _bValue;

	public BooleanLiteral(String sValue) {
		_bValue = sValue.equals("true");
	}

	@Override
	public String toString() {
		return (_bValue) ? "true" : "false";
	}

	@Override
	public void accept(ExpressionVisitor visitor) {
		visitor.visit(this);
	}
}
