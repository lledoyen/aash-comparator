package org.elegant.aash.comparator.parsing.expr;

import org.elegant.aash.comparator.parsing.visitor.ExpressionVisitor;

public abstract class Expression {
	public abstract String toString();

	public abstract void accept(ExpressionVisitor visitor);
}
