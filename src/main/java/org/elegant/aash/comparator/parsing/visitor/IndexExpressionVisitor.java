package org.elegant.aash.comparator.parsing.visitor;

import org.elegant.aash.comparator.parsing.expr.ArithmeticExpression;
import org.elegant.aash.comparator.parsing.expr.ArrayExpression;
import org.elegant.aash.comparator.parsing.expr.BooleanExpression;
import org.elegant.aash.comparator.parsing.expr.DottedExpression;
import org.elegant.aash.comparator.parsing.expr.FunctionCallExpression;
import org.elegant.aash.comparator.parsing.expr.NotExpression;
import org.elegant.aash.comparator.parsing.expr.literal.BooleanLiteral;
import org.elegant.aash.comparator.parsing.expr.literal.DoubleLiteral;
import org.elegant.aash.comparator.parsing.expr.literal.StarLiteral;
import org.elegant.aash.comparator.parsing.expr.literal.StringLiteral;

public class IndexExpressionVisitor implements ExpressionVisitor {

	private Double _dValue;
	private boolean _bStar;

	public void visit(DottedExpression compareDottedExpression) {
		// TODO Auto-generated method stub
		
	}

	public void visit(ArrayExpression compareArrayExpression) {
		// TODO Auto-generated method stub
		
	}

	public void visit(FunctionCallExpression compareFunctionCallExpression) {
		// TODO Auto-generated method stub
		
	}

	public void visit(NotExpression compareNotExpression) {
		// TODO Auto-generated method stub
		
	}

	public void visit(BooleanLiteral compareBooleanLiteral) {
		// TODO Auto-generated method stub
		
	}

	public void visit(DoubleLiteral compareDoubleLiteral) {
		_dValue = compareDoubleLiteral.getValue();
	}

	public void visit(StarLiteral compareStarLiteral) {
		_bStar = true;
	}

	public void visit(StringLiteral compareStringLiteral) {
		// TODO Auto-generated method stub
		
	}

	public void visit(BooleanExpression compareBooleanExpression) {
		// TODO Auto-generated method stub
		
	}

	public void visit(ArithmeticExpression compareArithmeticExpression) {
		// TODO Auto-generated method stub
		
	}

	public Double getDouble() {
		return _dValue;
	}

	public boolean isStar() {
		return _bStar;
	}
}
