package org.elegant.aash.comparator.parsing.visitor;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import org.elegant.aash.comparator.parsing.expr.DottedExpression;
import org.elegant.aash.comparator.parsing.visitor.model.PropertyElementValue;
import org.elegant.aash.comparator.tools.Lists;
import org.elegant.aash.comparator.tools.Objects;

public class PropertyExpressionVisitor implements DottedExpressionVisitor {

	private PropertyElementValue _graph;
	private List<PropertyElementValue> currentGraphNodes;

	private PropertyExpressionVisitor(Object oldBean, Object newBean) {
		// Init first element;
		_graph = new PropertyElementValue(oldBean, newBean);
		currentGraphNodes = Lists.newArrayList(_graph);
	}

	public static PropertyExpressionVisitor of(Object oldBean, Object newBean) {
		return new PropertyExpressionVisitor(oldBean, newBean);
	}

	public void visit(DottedExpression compareDottedExpression) {
		IndexExpressionVisitor indexVisitor = null;
		if(compareDottedExpression.getIndex() != null) {
			indexVisitor = new IndexExpressionVisitor();
			compareDottedExpression.getIndex().accept(indexVisitor);
		}
		boolean mustGoDeeper = false;
		for(PropertyElementValue currentGraphNode : currentGraphNodes) {
			// Assume all objects of the collections are of the same type which contains targeted method
			if (currentGraphNode.getOldInitialValue() != null) {
				currentGraphNode.setClazz(currentGraphNode.getOldInitialValue().getClass());
			} else if (currentGraphNode.getNewInitialValue() != null) {
				currentGraphNode.setClazz(currentGraphNode.getNewInitialValue().getClass());
			}
			currentGraphNode.setVarName(compareDottedExpression.getVariableName());
			if(compareDottedExpression.getIndex() != null) {
				currentGraphNode.setCondition(compareDottedExpression.getIndex().toString());
			}
			try {
				Method m = null;
				Object oldObject = null;
				Object newObject = null;
				if(currentGraphNode.getClazz() != null) {
					mustGoDeeper = true;
					m = currentGraphNode.getClazz().getMethod("get" + currentGraphNode.getVarName().substring(0, 1).toUpperCase() + currentGraphNode.getVarName().substring(1));
					if (currentGraphNode.getOldInitialValue() != null) {
						oldObject = m.invoke(currentGraphNode.getOldInitialValue());
					}
					if (currentGraphNode.getNewInitialValue() != null) {
						newObject = m.invoke(currentGraphNode.getNewInitialValue());
					}
					
					if (indexVisitor == null) {
						currentGraphNode.setNext(Lists.newArrayList(new PropertyElementValue(oldObject, newObject)));
					}else {
						currentGraphNode.setCondition(compareDottedExpression.getIndex().toString());
						if(indexVisitor.getDouble() != null) {
							currentGraphNode.setNext(Lists.newArrayList(new PropertyElementValue(Objects.safeGet(oldObject,indexVisitor.getDouble().intValue()) , Objects.safeGet(newObject,indexVisitor.getDouble().intValue()))));
						} else if(indexVisitor.isStar()) {
							List<PropertyElementValue> next = Lists.newArrayList();
							for(int i = 0; i < Objects.safeMaxSize(oldObject, newObject); i++) {
								next.add(new PropertyElementValue(Objects.safeGet(oldObject,i), Objects.safeGet(newObject,i)));
							}
							currentGraphNode.setNext(next);
						}
					}
				}
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				throw new IllegalStateException("Cannot access field[" + currentGraphNode.getVarName() + "] of class[" + currentGraphNode.getClazz() + "]", e);
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(mustGoDeeper && compareDottedExpression.getNext() != null) {
			List<PropertyElementValue> newNodes = Lists.newArrayList();
			for(PropertyElementValue currentGraphNode : currentGraphNodes) {
				if(currentGraphNode.getNext() != null) {
					newNodes.addAll(currentGraphNode.getNext());
				}
			}
			currentGraphNodes = newNodes;
			if(currentGraphNodes.size() > 0) {
				compareDottedExpression.getNext().accept(this);
			}
		}
	}

	public PropertyElementValue getGraph() {
		return _graph;
	}
}
