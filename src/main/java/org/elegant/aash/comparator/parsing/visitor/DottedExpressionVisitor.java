package org.elegant.aash.comparator.parsing.visitor;

import org.elegant.aash.comparator.parsing.expr.DottedExpression;

public interface DottedExpressionVisitor {

	void visit(DottedExpression compareDottedExpression);
}
